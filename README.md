1. Consume the following API and deserialize the JSON response : https://sheetsu.com/apis/v1.0su/78695974c3f1
2. With the data from this endpoint, display the title and image of each object in the response.
3. Run the application in the Emulator or on an Android device.
4. Ensure the app displays correctly when the orientation changes.
5. Submit your completed updates to new GitHub repo.

Bonus Points
• Implement the above requirements in Kotlin.